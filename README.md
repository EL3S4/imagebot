# imagebot

## Requisitos

- NodeJs
- Chromium
- npm

La instalación de chromium puede llevarse a cabo mediante el AUR, snap o pacman

## Soporte

Por el momento este proyecto solo ha sido provado en distribuciones linux, se agradece cualquier intento de port a Windows.

## Instalacion

### Clonar el repositorio

Se puede realizar dando al boton de descarga de esta pagina de GitLab o mediante el uso de la terminal con el comando:

```
git clone https://gitlab.com/EL3S4
```

### Ejecución

Una vez clonado el repositorio se debe acceder a la carpeta del mismo y ejecutar el archivo index.js mediante NodeJs

```
cd imagebot
```

```
node index.js
```

### IMPORTANTE

Recuerde vaciar la carpeta 'images' antes de cada ejecución, de lo contrario tendrá que clasificar las imagenes por su cuenta

## Licencia

Este proyecto esta sujeto a la licencia 'GNU AFFERO GENERAL PUBLIC LICENSE v3' publicada en 2007
