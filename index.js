const puppeteer = require('puppeteer');
const prompt = require('prompt');
const path = require('path');
const https = require('https');
const fs = require('fs');
const modules = require('./modules');

// imprimir banner de inicio
console.log('+=========================================+');
console.log('|INICIO DE PROGRAMA DE DESCARGA DE RULE 34|');
console.log('+=========================================+');

// definir propiedades de la url
var schema = {
    properties: {
        url:{
            hidden:false
        }
    }
};

prompt.start();

// parte principal del script
(async () => {
    // iniciar buscador web
    console.log('Iniciando buscador...');
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();

    // pedir los datos
    console.log('Introduzca la url...');
    var result = await new Promise(function (resolve, reject) {
        prompt.get(schema, function(err,result){
            resolve(result);
        });
    });

    // dirigirse a url
    await page.goto(result.url);
    console.log('Accediendo a la url dada...');
    await page.waitForSelector('body > div.tnc > p:nth-child(3) > a:nth-child(1)');

    // aceptar cookies
    await page.click('body > div.tnc > p:nth-child(3) > a:nth-child(1)');
    console.log('Aceptando cookies de la pagina...');
    await page.waitForTimeout(3000);
    await page.waitForSelector('#image-list');
    await page.evaluate(_ => { window.scrollBy(0, window.innerHeight) });
    
    // obtener pagina actual
    let pagURL = await page.url();
    const pag0 = modules.numPag(pagURL);
    console.log('Pagina inicial: ', pag0);

    // obtener ultima pagina
    if(pag0 == 1){
        await page.click('#paginator > div > a:nth-child(3)');
    } else {
        await page.click('#paginator > div > a:nth-child(5)');
    }
    
    await page.waitForTimeout(5000)
    let pageURL2 = await page.url();
    const pagF = modules.numPag(pageURL2);
    console.log('Pagina final: ', pagF);
    await page.goBack();

    // obtener numero de paginas y primer for
    for(var i = pag0; i <= pagF; i++){ //bucle de entrada a paginas
        console.log('Pagina numero: ', i); // imprimir numero de pagina
        const count =  (await page.$$('span.need-del')).length; // contar imagenes
        console.log('Imagenes detectadas en esta pagina: ', count);

        for(var j = 1; j <= count; j++){ //bucle de acceso a imagenes
            let imageSelector = '#image-list > div > div > div:nth-child('+j+') > a:nth-child(3)'; //generar el selector de la imagen
            await page.waitForSelector(imageSelector);
            await page.click(imageSelector); //acceder al selector
            let imgURL = await page.url(); //obtener la url de la imagen
            modules.descarga(imgURL); //descargar imagen
            await page.waitForTimeout(3000)
            await page.goBack(); //volver a la pagina de imagenes
        }
        await page.waitForSelector('#paginator > div');
        console.log('CAMBIANDO DE PAGINA');
        await page.waitForTimeout(3000);
        if(i == 1){
            await page.click('#paginator > div > a:nth-child(2)') //cambiar pagina si empiezas en el 1
        } else {
            if (i == pagF){
                break;
            }
            await page.click('#paginator > div > a:nth-child(4)') //cambiar pagina si empiezas en el 2
        }
        await page.waitForSelector('#image-list')
    }
    browser.close()
})()
