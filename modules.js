// Funcion para determinar el numero de pagina
function numPag(pagURL){
    const invertida = pagURL.split("").reverse().join("");
    let numInv = "";
    for( var i = 0; i < invertida.length; i++){
        var character = invertida.charAt(i);
        if (character == "/"){
            break;
        } else{
            numInv = numInv + character;
        }
    }
    const numSTR = numInv.split("").reverse().join("");
    const numINT = parseInt(numSTR);
    return numINT
}

// Funcion para la descarga de imagenes
const https = require('https');
const http = require('http');
const fs = require('fs');
const path = require('path');
const URL = require('url').URL;

function descarga(url){
    const userURL = new URL(url);
    const requestCaller = userURL.protocol === "http:" ? http : https;

    const nombre = path.basename(url)
    const direccion = "images/" + nombre;
    const req = requestCaller.get(url,function(res){
        const fileStream = fs.createWriteStream(direccion);
        res.pipe(fileStream);

        fileStream.on('error', function(err){
            console.log('Error writting');
            console.log(err);
        });

        fileStream.on('finish', function(err){
            fileStream.close();
            console.log('Imagen descargada...');
        });
    });

    req.on('error', function(err){
        console.log('Error de descarga');
        console.log(err);
    })
}
module.exports.descarga = descarga;
module.exports.numPag = numPag;